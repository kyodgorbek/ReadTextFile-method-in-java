# ReadTextFile-method-in-java



import java.io.*;

import javax.swing.JOptionPane;

public class ReadTextFile {
  
  public static void main(String[] args){ 
  
  try {
   // Create object ins - input file
   String inFileName = JOptionPane.showInputDialog("Input file name?");
   FileReader inStream = new FileReader(inFileName);
   
   // Create object outs - the output file
   String outFileName = JOptionPane.showInputDialog("Output file name?");
   FileWriter outStream = new FileWriter(outFileName);
   PrintWriter outs = new PrintWriter(outStream);
   
   // Reaf the number of data lines
   int numLines = Integer.parseInt(ins.readLine());
   
   // Read the data lines from the output file
   for (int lineCount = 0; lineCount < numLines; lineCount++) {
      String dataLine = ins.readLine();
      outs.println(dataLine);
     }
     
     // Write message to console and close files
     System.out.println(numLines + " data lines written to file " + outFileName);
     ins.close();
     outs.close();
   }
   catch (IOException ex){
   System.out.println("i/o error : " + ex.getMessage());
   ex.printStackTrace();
  }
  catch (NumberFormatException ex){
  System.out.println(ex.getMessage());
  }
 }      
} 
